import 'package:flutter/material.dart';
import 'package:zendesk/zendesk.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      home: HomePage(),
    );
  }
}

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          mainAxisSize: MainAxisSize.min,
          children: [
            MaterialButton(
                child: const Text("Agent one"),
                onPressed: () async{
                  await ZendeskHelper.initialize("IsnHbDn9XCExGvclZ46PtyV3qaDPR37O", "50a70829126d9d19c7a4e1e02340c5833c48d34c83636988");
                      await ZendeskHelper.setVisitorInfo(
                        name: "Agent 1",
                        phoneNumber: "09999999999",
                      );
                      await ZendeskHelper.startChat(
                        primaryColor: Colors.red
                      );
                }),
            const SizedBox(height: 10,),
            MaterialButton(
                child: const Text("Agent two"),
                onPressed: () async{
                  await ZendeskHelper.initialize("IsnHbDn9XCExGvclZ46PtyV3qaDPR37O", "50a70829126d9d19c7a4e1e02340c5833c48d34c83636988");
                  await ZendeskHelper.setVisitorInfo(
                    name: "Agent 2",
                    phoneNumber: "088888888",
                  );
                  await ZendeskHelper.startChat(
                      primaryColor: Colors.red
                  );
                }),
            const SizedBox(height: 10,),
            MaterialButton(
                child: const Text("Agent three"),
                onPressed: () async{
                  await ZendeskHelper.initialize("IsnHbDn9XCExGvclZ46PtyV3qaDPR37O", "50a70829126d9d19c7a4e1e02340c5833c48d34c83636988");
                  await ZendeskHelper.setVisitorInfo(
                    name: "Agent 3",
                    phoneNumber: "077777777",
                  );
                  await ZendeskHelper.startChat(
                      primaryColor: Colors.red
                  );
                })
          ],
        ),
      ),
      // body: Center(child: GestureDetector(
      //   onTap: () async {
      //     await ZendeskHelper.initialize("IsnHbDn9XCExGvclZ46PtyV3qaDPR37O", "50a70829126d9d19c7a4e1e02340c5833c48d34c83636988");
      //     await ZendeskHelper.setVisitorInfo(
      //       name: "Miiiii",
      //       phoneNumber: "0099089",
      //     );
      //     await ZendeskHelper.startChat(
      //       primaryColor: Colors.red
      //     );
      //   },
      //   child: Text("Start chat"),
      // ),),
    );
  }
}
